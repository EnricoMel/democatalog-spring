package it.testspring.democatalog.web.controller;

// import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.testspring.democatalog.web.dao.ProdottoService;
import it.testspring.democatalog.web.model.Prodotto;

@Controller
@RequestMapping("/")
public class ProdottoController {
	
	@Autowired
	private ProdottoService service;
	
	@GetMapping("/")
	public ModelAndView index(@RequestParam(name = "id", required = false)String idProdotto, ModelMap mm) {
		// List<Prodotto> prodotti = service.getAll();
		
		if(idProdotto != null) {
			mm.addAttribute("prodottoEdit", service.getById(Integer.parseInt(idProdotto)));
		}
		
		return new ModelAndView("index", "listaProdotti", service.getAll());
	}
	
	@PostMapping("/add")
	public String add(@ModelAttribute("dati")Prodotto p) {
		
		service.add(p);
		
		return "redirect:/";
	}
	
	@PostMapping("/update")
	public String update(@ModelAttribute("dati")Prodotto p) {
		
		service.update(p);
		
		return "redirect:/";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id")String idProdotto) {
		
		if(idProdotto != null)
			service.delete(Integer.parseInt(idProdotto));
		
		return "redirect:/";
	}
	
}
