package it.testspring.democatalog.web;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import it.testspring.democatalog.web.dao.ProdottoService;
import it.testspring.democatalog.web.dao.ProdottoServiceImpl;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "it.testspring.democatalog.web.controller")
@PropertySource("classpath:demo-catalog.properties")
@EnableTransactionManagement
public class DemoCatalogConfig {
	
	@Autowired
	private Environment env;
	
	@Bean
	public FreeMarkerViewResolver getFreeMarkerResolver() {
		FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
		resolver.setCache(true);
		resolver.setPrefix("");
		resolver.setSuffix(".ftl");
		
		return resolver;
	}
	
	@Bean
	public FreeMarkerConfigurer getFreeMarkerConfigurer() {
		FreeMarkerConfigurer config = new FreeMarkerConfigurer();
		config.setTemplateLoaderPath("/WEB-INF/view/");
		
		return config;
	}
	
	@Bean
	public DataSource getDbConnection() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(env.getRequiredProperty("demo-catalog.db.driver"));
		ds.setUsername(env.getRequiredProperty("demo-catalog.db.username"));
		ds.setPassword(env.getRequiredProperty("demo-catalog.db.password"));
		ds.setUrl(env.getRequiredProperty("demo-catalog.db.url"));
		
		return ds;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean getEntityManager() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabase(Database.MYSQL);
		adapter.setGenerateDdl(true);
		
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(getDbConnection());
		factory.setJpaVendorAdapter(adapter);
		factory.setPackagesToScan(getClass().getPackage().getName());
		
		return factory;
	}
	
	@Bean
	public PlatformTransactionManager getTransactionManager() {
		return new JpaTransactionManager(getEntityManager().getObject());
	}
	
	@Bean
	public ProdottoService getProdottoService() {
		return new ProdottoServiceImpl();
	}
	
}
