<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<title>Welcome to demo-catalog!</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	</head>
	
	<!-- New products -->
	<body class="bg-light">
		<h1 class="text-center my-3">Demo-Catalog!</h1>
		
		<#if prodottoEdit??>
		
			<div class="container bg-info-subtle">
			<div class="row">
				<div class="col-12">
					<h3 class="text-center mb-3">Modifica prodotto - ${prodottoEdit.nome}</h3>
				
					<form method="POST" action="update" id="dati">
					<input type="hidden" name="id" value="${prodottoEdit.id}"/> 
					  <div class="mb-3">
					    <label for="nome" class="form-label">Nome</label>
					    <input type="text" name="nome" value="${prodottoEdit.nome}" id="nome" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					  </div>
					   <div class="mb-3">
					    <label for="descrizione" class="form-label">Descrizione</label>
					    <input type="text" name="descrizione" value="${prodottoEdit.descrizione}" id="descrizione" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					  </div>
					   <div class="mb-3">
					    <label for="prezzo" class="form-label">Prezzo</label>
					    <input type="number" name="prezzo" value="${prodottoEdit.prezzo}" id="prezzo" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					  </div>
					  <button type="submit" name="invia" value="Salva modifiche" class="btn btn-primary">Salva Modifiche</button>
					</form>
					
				</div>
			</div>
		  </div>
		  
		<#else>
		
			<div class="container bg-info-subtle">
			<div class="row">
				<div class="col-12">
					<h3 class="text-center mb-3">Inserisci un nuovo prodotto</h3>
				
					<form method="POST" action="add" id="dati">
					  <div class="mb-3">
					    <label for="nome" class="form-label">Nome</label>
					    <input type="text" name="nome" value="" id="nome" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					  </div>
					   <div class="mb-3">
					    <label for="descrizione" class="form-label">Descrizione</label>
					    <input type="text" name="descrizione" value="" id="descrizione" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					  </div>
					   <div class="mb-3">
					    <label for="prezzo" class="form-label">Prezzo</label>
					    <input type="number" name="prezzo" value="" id="prezzo" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					  </div>
					  <button type="submit" name="invia" value="Aggiungi" class="btn btn-primary">Submit</button>
					</form>
					
				</div>
			</div>
		  </div>
		  
		</#if>
		
		<hr>
		
		  <!-- List
		  <div class="container my-5">
		  <h3 class="text-center mb-5">Lista prodotti</h3>
		  		<div class="row">
		  			<#list listaProdotti as prodotto>
		  			<div class="col-12 col-md-2">
					  	<div class="card" style="width: 18rem;">
						   <img src="https://picsum.photos/100/100" class="card-img-top" alt="...">
						    <div class="card-body">
							<h5 class="card-title">${prodotto.nome}</h5>
						    <p class="card-text">${prodotto.descrizione}</p>
						    <p class="card-text">${prodotto.prezzo}</p>
							<a href="delete?id=${prodotto.id}" class="btn btn-primary">Elimina</a>
							<a href="?id=${prodotto.id}" class="btn btn-primary">Modifica</a>
					 	</div>	 
					</div>
					</#list>
				</div>
			</div> -->
					
			
		  
		  <!-- List -->
		  <div class="container my-3 bg-warning-subtle">
		  	<div class="row">
		  		<div class="col-12">
		  			<h3 class="text-center">Lista prodotti</h3>
		  			<table>
		  				<thead>
		  					<tr>
		  						<th>Nome</th>
		  						<th>Descrizione</th>
		  						<th>Prezzo</th>
		  						<th>Azioni</th>
		  					</tr>
		  				</thead>
		  				<tbody>
		  				<#list listaProdotti as prodotto>
		  					<tr>
		  						<td>${prodotto.nome}</td>
		  						<td>${prodotto.descrizione}</td>
		  						<td>${prodotto.prezzo}</td>
		  						<td>
		  							<a href="delete?id=${prodotto.id}">Elimina</a>
		  							<a href="?id=${prodotto.id}">Modifica</a>
		  						</td>
		  					</tr>
		  				</#list>
		  				</tbody>
		  			</table>
				</div>
			</div>
		  </div>
		
		
		
		
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.7/dist/umd/popper.min.js" integrity="sha384-zYPOMqeu1DAVkHiLqWBUTcbYfZ8osu1Nd6Z89ify25QV9guujx43ITvfi12/QExE" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.min.js" integrity="sha384-Y4oOpwW3duJdCWv5ly8SCFYWqFDsfob/3GkgExXKV4idmbt98QcxXYs9UoXAB7BZ" crossorigin="anonymous"></script>
	
	</body>
	
</html>